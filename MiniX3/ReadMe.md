# MiniX3 - Throbber

![Eksempel på Minix3](https://gitlab.com/lukas-ap/ap/-/raw/main/MiniX3/ezgif.com-video-to-gif-converter.gif)

[RunMe](https://lukas-ap.gitlab.io/ap/MiniX3)

[ReadMe](https://gitlab.com/lukas-ap/ap/-/blob/main/MiniX3/sketch.js)

### **What have i produced?**
In this MiniX assignment, we had to create a throbber, and after seeking inspiration in retro games such as Golden eye, Need for speed, resident evil etc. i realised that many of the old console games, uses a loading screen, instead of a throbber. Proberbly since the controller interface on consoles, made it hard to visualize a throbber because of the (often) lack of cursor on the GUI.
Therefore i decided to look elsewhere for my inspiration, and remembered the old windows 7 throbber, which i remember fondly. Growing up in the 2010s, windows 7 had a huge impact on the digital culture i grew up exploring, and learing from, so i instantly knew i wanted to create a modernized version of it!

![Windows7 Throbber](https://gitlab.com/lukas-ap/ap/-/raw/main/MiniX3/windows7loading.gif)

### **My code**

I started my code by defining 2 set of values named *num* and *num1*, i will use theese later for my loops, i also defined an array called *loadingPhrases* which i will use for my.. well.. loading phrases later.

In my *loadingPhrases* i filled up the array with different phrases to be displayed underneath the Throbber to further implicate that *something* is loading, e.g 
*'Loading. Loading.. Loading... Please wait. Please wait.. Please wait...'*


In my setup i set the *framerate()* to 20 frames per second, and the alpha value on my background to 35. I tinkered a lot with theese in order to create a smooth, but not to fast animation effect on my Throbber. I discovered that if my framerate was too high, the animation would be way to fast, and if the framerate was to low, the animation would be very chunky, so i felt 20 FPS was the perfect middleground. Ideally i would have preffered a higher framerate, that would make the throbber more smooth in is animation, but the speed of the animation created a more *high stress* feeling, that i didn't feel like would go well with the throbbers purpose.


For my Thobber animation i used a long *If statement* that used the *'let num 0;'* i defined in the beginning. The if statement checks the value of *'num'* and creates a set of ellipses with different radius' along with the increasing *'num'* value, that is ever increasing because of line 70: *'num++;'*, and caps out and restarts at 15 because of the if statement in line 72: *'  if (num > 15) { num = 0;}'* 

Lastly i implemented the array i defined in the beginning *'loadingPhrases'*.
the first part of this function *'floor(num1 / 30):'*  divides num1 by 30, essentially defining num1 for 30 frames per number. E.g if the num1 = 0-29, the result will be 0; 30-59 the result will be 1, and so on... The next part *'% loadingPhrases.length'* calculates the 'remainder' of the result, allowing it to loop around, and start over, when the limit is reached, effectively creating an infinite loop of the loadingPhrases!


### **Reflections**

In todays digital age, we are used to have instant access to information from all around the world wide web, and the throbber is often met with frustrations, because of the lack of access... However with my modern representation of the Windows 7 throbber, i want to throw the user back to the 2010s, where your internet access was prohibited to the schools laptop or your parents office computer. The loading phrases enhances the user feedback during loading times, and the phrases becomming more and more demotivating the longer you wait, eventually displaying *'Just give up..., it will never finish loading'*. I clearly remeber loading a game on my parents computer and watching the loading bar barely move, having to go outside and play, and come back to check on the progress every half an hour or so. eagerly looking forward to try the game.. which i sometimes realised after less that 5 minutes was way to boring or complex for my attention span. The Throbber back then, for me, was a symbol of excitment, and patience, that made me appreciate the game, image, video... that was being loaded. 

This also creates an appreciation of how fast everything is today, and how far everything has come. Instead of having a loading screen, like on retro game consoles, we now have a throbber for our cursor that we only see once in a while when an application crashes, or we are checking our taxes.


___

### **References**

https://p5js.org/reference/

https://archive.org/details/Windows7CursorPack