let num = 0;
let num1 = 0;
let loadingPhrases = ['Loading.', 'Loading..', 'Loading...', 'Loading.', 'Loading..', 'Loading...', 'Please wait.', 'Please wait..', 'Please wait...', 'Please wait.', 'Please wait..', 'Please wait...', 'Be patient.', 'Be patient..', 'Be patient...', 'Be patient.', 'Be patient..', 'Be patient...', 'Almost there.', 'Almost there..', 'Almost there...', 'Almost there.', 'Almost there..', 'Almost there...', 'Just give up.', 'Just give up..', 'Just give up...', 'Just give up.', 'Just give up..', 'Just give up...', 'It will never finish loading.', 'It will never finish loading..', 'It will never finish loading...', , 'It will never finish loading.', 'It will never finish loading..', 'It will never finish loading...'];

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(20);
}

function draw() {
  background(255, 35);
  drawElements();
}

function drawElements() {
  push();
  translate(width / 2, height / 2);
  noStroke();
  fill(137, 207, 240);
  if (num >= 0) {
    ellipse(0, 0, 20, 20);
  }
  if (num >= 1) {
    ellipse(0, 0, 30, 30);
  }
  if (num >= 2) {
    ellipse(0, 0, 40, 40);
  }
  if (num >= 3) {
    ellipse(0, 0, 50, 50);
  }
  if (num >= 4) {
    ellipse(0, 0, 60, 60);
  }
  if (num >= 5) {
    ellipse(0, 0, 70, 70);
  }
  if (num >= 6) {
    ellipse(0, 0, 80, 80);
  }
  if (num >= 7) {
    ellipse(0, 0, 90, 90);
  }
  if (num >= 8) {
    ellipse(0, 0, 100, 100);
  }
  if (num >= 9) {
    ellipse(0, 0, 110, 110);
  }
  if (num >= 10) {
    ellipse(0, 0, 120, 120);
  }
  if (num >= 11) {
    ellipse(0, 0, 130, 130);
  }
  if (num >= 12) {
    ellipse(0, 0, 140, 140);
  }
  if (num >= 13) {
    ellipse(0, 0, 150, 150);
  }
  if (num >= 14) {
    ellipse(0, 0, 160, 160);
  }
  if (num >= 15) {
    ellipse(0, 0, 170, 170);
  }
  pop();

  num++;

  if (num > 15) {
    num = 0;
  }
  drawLoadingText();
}

function drawLoadingText() {
  push();
  translate(width / 2, height / 2);
  fill(0);
  textAlign(CENTER);

  let loadingIndex = floor(num1 / 30) % loadingPhrases.length;
  text(loadingPhrases[loadingIndex], 0, 120);
  
  pop();
  
  num1++;
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}