# MiniX02 - Emoji

![Eksempel på MiniX02](https://gitlab.com/lukas-ap/ap/-/raw/main/MiniX2/GIF.gif)

[Click here to RunMe!](https://lukas-ap.gitlab.io/ap/MiniX2)

[Click here to ReadMe!](https://gitlab.com/lukas-ap/ap/-/blob/main/MiniX2/sketch.js)


### **What have i produced?**

For this MiniX, I explored P5JS' 3D space with a 3D *ellipsoid* that rotates around its Y-axis and changes texture when pressed. To achieve the desired goal of a texture change when the *ellipsoid* is clicked, i initially set the variable isTextureChanged to false. This variable is then used to determine which texture to apply to the ellipsoid.

>if (isTextureChanged) {
        texture(textureImg2);
    } else {
        texture(textureImg);
    }

This *if statement* checks if isTextureChanged is true. If it is, it applies *textureImg2* to the *ellipsoid*; otherwise, it applies *textureImg*.

For my initial emoji, I used a PNG picture to texture my *ellipsoid* and created a function *mousePressed* that tracks if the mouse position is within 200 pixels from the center of the canvas. If pressed within this border, it changes the *isTextureChanged = true*, changing the texture to *'TextureImg2'*.

>function mousePressed() {
    let d = dist(mouseX, mouseY, width / 2, height / 2);
    if (d < 200) {
        isTextureChanged = true;
    }
}

To return the original *TextureImg* after releasing the mouse button, I used this function.

> function mouseReleased() {
    isTextureChanged = false;
}


### **How would you put emojis into a wider social and cultural context?** 

Emojis have become deeply embedded into our modern communications, and can have a big impact on how we visualize emotions, objects and people, thus alos shaping how we might view the world. With this in mind, the design of emojis play an important role in representing various identities. Incluisivity in emoji design is essential for individuals to see themselves, creating a sense of belonging.

Doing this MiniX, i spent a long time reflecting on how it would be possible to make an emoji, that lived up to this inclusivity, while being mindful of cultural appropiation. Doing my research i stumbled upon different articles, showcasing how emojis have evolved over the years. Many Emojis have been critizied for their lack of racial diversity, predomiantly featuring white characters with limited options for people of color.

After looking at *Artem Loenko's* article at [*uxdesign.cc - How emojis evovle over time*](https://uxdesign.cc/emojis-over-time-6a10c9f1d288) where Artem compared what changes Apple's emojis have gone through, i actually decided to use two of apples own emojis. It is nice to see, that companies are designing for inclusivity, but there is still a long way to go.


___

### **References**
Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020

https://www.wired.co.uk/article/emoji-gender-inclusive-unicode

https://uxdesign.cc/emojis-over-time-6a10c9f1d288

https://p5js.org/reference/





