let backgroundImg;
let textureImg;
let textureImg2;
let isTextureChanged = false;

function preload() {
    backgroundImg = loadImage('backgroundimg.jpg');
    textureImg = loadImage('Eye_Roll_Emoji_large.png');
    textureImg2 = loadImage('surprised_emoji.png')
}

function setup() {
    createCanvas(windowWidth, windowHeight, WEBGL);
    image(backgroundImg, -1000, -500)
}

function draw() {
    rotateY(millis() / 1000);
    if (isTextureChanged) {
        texture(textureImg2);
    } else {
        texture(textureImg);
    }
    noStroke();
    ellipsoid(200, 200, 200);

}

function mousePressed() {
    let d = dist(mouseX, mouseY, width / 2, height / 2);
    if (d < 200) {
        isTextureChanged = true;
    }
}

function mouseReleased() {
    isTextureChanged = false;
}