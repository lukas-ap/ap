# Sisyphus - MiniX01

![Eksempel på minix1](https://gitlab.com/lukas-ap/ap/-/raw/main/MiniX1/MiniX1.png)

[Click here to RunMe!](https://lukas-ap.gitlab.io/ap/MiniX1) (If it doesn't work the first time, refresh the page ;)

[Click here to ReadMe!](https://gitlab.com/lukas-ap/ap/-/blob/main/MiniX1/sketch.js?ref_type=heads)


### **What Have I Produced?**

For my first MiniX, I decided to challenge myself by creating an endless loop, and therefore came up with the idea of creating an animation depicting Sisyphus's eternal task of pushing a stone uphill.
I started by creating a simple canvas with dimensions *'windowHeight'* times *'windowWidth'*. Following the instructions from the p5.js reference tab on images, I learned about the *'loadImage()'* function, which allowed me to load images and videos into my code. After encountering some difficulties with loading the images, I began troubleshooting the problems and added callbacks that would notify me in the console if my image and/or video was loaded successfully.

>() => {
    console.log("Image loaded successfully.");
  }, () => {
    console.log("Error loading image.");
  };

In addition to my picture and video, I also added a rectangle, which I angled to resemble a mountain. Lastly, due to an error from Google Chrome that prevented the video from playing directly on opening, I added a *'Click here to start!'* button that would initiate the video loop.


### **How would i describe my first independent coding experience?**

After having learned the basics of HTML, CSS, and JavaScript and spending a lot of time in our Tamagotchi exercise, in an exciting, fun, and occasionally frustrating way, my first coding experience with p5.js has been largely positive. The reference list in p5.js has been a big help, and the language itself seems generally more intuitive to use, and i'm excited to continue exploring it further.
Starting this course with next to none coding experience, being able to code stuff like this after only a couple of weeks, fuels my hunger to continue to learn and experiment with my coding.


### **Reflections**

Being able to share your vision with other people can be very challanging, but P5JS is an amazing tool to get your vision from your head, to a concrete example, that can easly be manipulated and worked on. This exercise helped me do that, going into the exercise i initially wanted to create and animations in the background, but through trial and talking, i came up with the solution of a video looping, showing that there often are more than one solution in coding, which just makes coding an even more complex tool!


---
### **References**
- Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-48
- Reference p5.js - https://p5js.org/reference/#/p5/
