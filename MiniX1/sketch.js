let img;
let video;
let value = 0;

function preload() {
  img = loadImage('ROLLINGSTONE.png', () => {
    console.log("Image loaded successfully.");
  }, () => {
    console.log("Error loading image.");
  });
  video = createVideo('daynightcycle.mp4', () => {
    console.log("Video loaded successfully.");
  });
  video.hide();
  video.loop();
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(254, 254, 254);
}


function draw() {
  fill(102, 153, 204);
  rect(100, 150, 300, 100);
  fill(254,254,254)
  
  text('Click here to start!', 190, 205, )

  image(video, 0, 0, 1920, 1080)

  translate(width / 2, height / 2);
  rotate(PI / -6.0);
  fill(0, 0, 0);
  rect(-900, 100, 2000, 1000);

  circle(200, 130, 80, 80)
  circle(700, 130, 100, 50)

  rotate(PI / 10)
  image(img, -400, -230);
}

function mousePressed() {
  if (mouseX > 100 && mouseX < 400 && mouseY > 150 && mouseY < 250) {
    video.play();
  }
}
