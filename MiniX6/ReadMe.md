# MiniX6 - FLYING CANNIBAL DONUT MASSACRE THE GAME V.1.0.0.1

![Eksempel på MiniX6](https://gitlab.com/lukas-ap/ap/-/raw/main/MiniX6/data/FCDMTGEXAMPLE.gif)
                                                            
[Click here to play!](https://lukas-ap.gitlab.io/ap/MiniX6)

[Click here to read my code!](https://gitlab.com/lukas-ap/ap/-/blob/main/MiniX6/sketch.js)


**THE GAME MIGHT TAKE A WHILE TO LOAD THE FIRST TIME YOU OPEN IT**


## **How does the game work**
I have created a game that involves controlling a 'blob' with your mouse, while trying to eat all the smaller blobs on the canvas. When you collide with a 'pointBlob'*(smaller blob)* with your main blob, the main blob 'eats' the pointBlob and thus increases in size with `this.r += other.r * 0.3;` (or 30%). this will continue until you have eaten all the *pointBlobs* on the canvas *(300 by default)*, by which an endgame screen will show, displaying how long time it took you to eat all the pointBlobs.

The most important featue of this game is the main blob 'eating' the pointBlobs, i managed to do this with an if statement: `if (d < this.r + other.r)`, which calculates if the distance between the blobs is smaller than the radius of the main blob + the radius of the pointBlob (meaning they are colliding)

The game consists of 3 scenes; the first scene is an opening game screen, making sure that the game don't start directly on startup, and allows the player to start the game manually when ready, with a keyPressed function. The next scene is the game itself, a canvas, with constraints (so you can't fly away), with 300 pointBlobs randomly put on the canvas within the constraints. and the last scene is an endgame screen, displaying how long time it took the player to finish the game.

In my keyPressed scene changer, i also implemented the startTime function, that will start counting seconds until the endTime is called in the endGame function. I had some problems getting the time to actually stop once the game finished, so i had to add an if statement `if (timeTaken === 0) let endTime = millis() timeTaken = endTime - startTime;` that ensures that the function is only called one time, and keeps displaying the time taken between pressing 'ENTER' and 'eating' all the pointBlobs.
The canvas is created with the dimensions *windowHeight x windowWidth* and is fully covered with an 8bit sky background, which is always moving in the different direction than the movement of the blob, in order to create a sense of movement through the ski, i managed to do this with the translate function `translate(-blob.pos.x, -blob.pos.y);`

In my game there are two main types of game objects: Blob, and pointBlob.

- **Blob:**
The blob class represents the main character and the point blobs in the game.
the blob class has theese methods:
    - update(): Updates the position of the blob based on the mouse movement. It calculates a new velocity vector towards the mouse cursor and smoothly transitions the blob towards that direction.
    - eats(other): Detects collisions between the main character and other blobs (point blobs). If a collision occurs, it increases the main character's size and returns true, indicating a successful eat.
    - show(): Displays the blob on the canvas at its current position

- **pointBlob:**
the pointBlobs are instances of the Blob class but represent the point blobs scattered throughout the game world.
They are randomly generated at the beginning of the game and are displayed on the canvas.
When the main character (blob) collides with a point blob, it gets eaten, increasing the main character's size and score.


## **Reflections on the game**

This game has taken big inspirations from both old-school 8-bit games, aswell as newer flash games. For example most of the core game mechanics e.g. movement, and the abosrbing feature, are both recreated from the popular flash game agar.io, however the feel of the game, especially in the looks are hugely impacted by oldschool 8-bit games, i also tried to recreate the oldschool *arcade* feel, with the speedrun element with the addition of a timer, allowing you to challange yourself and others to beat your highscores. Having more time i would have loved to implement a scoreboard, keeping track of the top scores set locally on each machine hosting the game.

Overall i am happy with the feel and look of the game, in the future i would love to brainstorm options to create a more varied game experience, as the game is largely the same each playthrough, despite of the 'random' function having a key role in the code.


## **References**
- https://p5js.org/reference/
- https://www.youtube.com/watch?v=JXuxYMGe4KI
- https://editor.p5js.org/pippinbarr/sketches/bjxEfpiwS
- https://editor.p5js.org/allison.parrish/sketches/qpOzlUy23
- agar.io
