let blob;
let pointBlobs = [];
let zoom = 1;
let backgroundImg;
let score = 0;
let scene = 0;
let font;
let string = 'Press enter to start...';
let currentCharacter = 0;
let donutImages = [];
let startTime;
let timeTaken = 0;

function preload() {
  font = loadFont('8bitFont.ttf')
  backgroundImg = loadImage('skybackground.jpeg')
  for (let i = 0; i < 3; i++) {
    donutImages[i] = loadImage('donut' + i + '.png');
  }
}


class Blob {
  constructor(x, y, r) {
    this.pos = createVector(x, y);
    this.r = r;
    this.vel = createVector(0, 0);
    this.img = random(donutImages)

    this.update = function () {
      let newvel = createVector(mouseX - width / 2, mouseY - height / 2);
      this.vel.setMag(1);
      this.vel.lerp(newvel, 0.01);
      this.pos.add(this.vel);
    }
    this.eats = function (other) {
      let d = p5.Vector.dist(this.pos, other.pos);
      if (d < this.r + other.r) {
        this.r += other.r * 0.3;
        return true;
      } else {
        return false;
      }
    }

    this.show = function () {
      image(this.img, this.pos.x - this.r, this.pos.y - this.r, this.r * 2, this.r * 2);
    }
  }
}


function setup() {
  createCanvas(windowWidth, windowHeight);
  let spawnCoordinateX = backgroundImg.width / 2;
  let spawnCoordinateY = backgroundImg.height / 2;
  blob = new Blob(spawnCoordinateX, spawnCoordinateY, 64);
  for (let i = 0; i < 300; i++) {
    let x = random(0, 6400);
    let y = random(0, 2700);
    pointBlobs[i] = new Blob(x, y, 16);
  }

}


function draw() {
  console.log("Score:", score);
  console.log("Scene:", scene);
  if (score < 300) {
  if (scene === 0) {
    let backgroundImgPosX = 0 - blob.pos.x;
    let backgroundImgPosY = 0 - blob.pos.y;
    image(backgroundImg, backgroundImgPosX, backgroundImgPosY);

    fill(255);
    textFont(font);
    textSize(200);
    textAlign(CENTER, TOP);
    text('WELCOME TO', width / 2, 60);

    fill(255);
    textFont(font);
    textSize(72);
    textAlign(CENTER, TOP);
    text('Flying Cannibal Donut Massacre The Game v. 1.0.0.1', width / 2, 240);

    let currentString = string.substring(0, currentCharacter);

    push();
    textSize(map(sin(frameCount*0.1), -1, 1, 90, 100));
    textAlign(CENTER, CENTER)
    textFont(font);
    text(currentString, width / 2, 550);
    currentCharacter += 0.25;
    pop();
  } else if (scene === 1) {
    let backgroundImgPosX = 0 - blob.pos.x;
    let backgroundImgPosY = 0 - blob.pos.y;

    // Draw the background image
    image(backgroundImg, backgroundImgPosX, backgroundImgPosY);
    // Apply transformations
    translate(width / 2, height / 2);
    let newzoom = 96 / blob.r;
    zoom = lerp(zoom, newzoom, 0.2);
    scale(zoom);

    let minX = 0;
    let maxX = 6400;
    let minY = 0;
    let maxY = 2700;

    blob.pos.x = constrain(blob.pos.x, minX, maxX);
    blob.pos.y = constrain(blob.pos.y, minY, maxY);

    translate(-blob.pos.x, -blob.pos.y);

    // Draw the central blob
    blob.show();
    blob.update();

    // Draw the pointBlobs
    for (let i = pointBlobs.length - 1; i >= 0; i--) {
      push();
      if (blob.eats(pointBlobs[i])) {
        pointBlobs.splice(i, 1);
        score++;
      }
      if (pointBlobs[i]) {
        pointBlobs[i].show();
      }
      pop();
    }

    resetMatrix();
    textFont(font);
    textSize(32);
    textAlign(CENTER, TOP);
    fill(255);
    text('Donuts eaten: ' + score, width / 2, 10);
  }
} else {
  let backgroundImgPosX = 0 - blob.pos.x;
  let backgroundImgPosY = 0 - blob.pos.y;
  image(backgroundImg, backgroundImgPosX, backgroundImgPosY);


  fill(255);
  textFont(font);
  textSize(100);
  textAlign(CENTER, TOP);
  text('Congratz! you ate all the donuts!', width / 2, 30);

  fill(255);
  textFont(font);
  textSize(100);
  textAlign(CENTER, TOP);
  text('In a time of:', width / 2, 250);

  fill(255);
  textFont(font);
  textSize(map(sin(frameCount*0.1), -1, 1, 200, 220));
  textAlign(CENTER, TOP);
  text((timeTaken / 1000).toFixed(2) + ' ' + 'seconds', width / 2, 400);

  fill(255);
  textFont(font);
  textSize(100);
  textAlign(CENTER, TOP);
  text('Refresh to try and beat your time!', width / 2, 700);

} if (score === 300) {
  endGame(); // Call endGame function when all donuts are eaten
}
}


function keyPressed() {
  if (keyCode === ENTER && scene === 0) {
    scene = 1;
    startTime = millis();
  }
}

function endGame() {
  if (timeTaken === 0) {
    let endTime = millis(); // Record the end time
    timeTaken = endTime - startTime; // Calculate the time taken
  }
}