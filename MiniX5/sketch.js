let step = 10;
let lines = [];
let song;

function preload (){
  soundFormats('mp3')
  song = loadSound('song.mp3')
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(255);
  lines = [];  // Clear the lines array

  // Create the lines
  for (let i = step; i <= height - step; i += step) {
    let line = [];
    for (let j = step; j <= width - step; j += step) {
      let distanceToCenter = Math.abs(j - width / 2);
      let variance = Math.max(width / 2 - 50 - distanceToCenter, 0);
      let random = Math.random() * variance / 20 * -1;
      let point = createVector(j, i + random);
      line.push(point);
    }
    lines.push(line);
  }

  // Draw lines
  strokeWeight(2);
  stroke(0);
  for (let i = 0; i < lines.length; i++) {
    for (let j = 0; j < lines[i].length - 1; j++) {
      let p1 = lines[i][j];
      let p2 = lines[i][j + 1];
      line(p1.x, p1.y, p2.x, p2.y);
    }
  }

}

function draw() {
}

function keyPressed() {
  if (keyCode === ENTER) {
    if (song && !song.isPlaying()) {
      song.loop();  // Play the sound in a loop
    }
  }
}

// Set interval to restart the sketch every 15 seconds
setInterval(() => {
  setup();
}, 500);

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}