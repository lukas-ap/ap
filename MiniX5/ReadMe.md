# MiniX4 - A generative program

![Eksempel på MiniX5](https://gitlab.com/lukas-ap/ap/-/raw/main/MiniX5/MiniX5_eksempel.PNG)
                                                                      
[RunMe](https://lukas-ap.gitlab.io/ap/MiniX5)

[Readme](https://gitlab.com/lukas-ap/ap/-/raw/main/MiniX5/sketch.js)


### What have i created?
I have created a generative program, with the main rule of creating a series of lines with a 'wavy' pattern, symbolising soundwaves. Each line consist of a series of points where the horizontal position of each point varies randomly to create the 'wavy' effect.

The variance in the horizontal position of the points depends on their distance from the center of the canvas. Points closer to the center have more variance, while points closer to the center have more variance.

This program is created from the combination of random variance in the horizontal position of the points and the variance in which points approach the center of the canvas.
This combination creates a visually interesting wavy pattern where the lines appear denser towards the edges and become more spread out towards the center of the canvas.
Additionally, since the variance is controlled by a random factor, each run of the program will produce a slightly different wavy pattern, leading to emergent variation in the appearance of the generated lines.