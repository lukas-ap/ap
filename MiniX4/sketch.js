let scene = 0; 

let faceTracker;
function createFaceTracker() {
  faceTracker = new clm.tracker();
  faceTracker.init(pModel);
  faceTracker.start(videoCapture.elt);
}

let videoCapture;
function createVideoCapture() {
  videoCapture = createCapture(VIDEO);
  videoCapture.size(640, 480);
  videoCapture.hide();
}



function getEmotion(positions) {
  const happyLandmarks = [44, 50, 57];
  const sadLandmarks = [44, 57]; // Indices of mouth landmarks for sadness detection

  // Check for "Happy" emotion
  if (positions[happyLandmarks[0]] && positions[happyLandmarks[1]] && positions[happyLandmarks[2]]) {
    if (positions[happyLandmarks[0]][1] < positions[happyLandmarks[2]][1] &&
      positions[happyLandmarks[1]][1] < positions[happyLandmarks[2]][1]) {
      return 'Happy';
    }
  }

  // Check for "Sad" emotion
  if (positions[sadLandmarks[0]] && positions[sadLandmarks[1]]) {
    const mouthY1 = positions[sadLandmarks[0]][1];
    const mouthY2 = positions[sadLandmarks[1]][1];
    if (mouthY1 < mouthY2) {
      return 'Sad';
    }
  }

  // If none of the above conditions are met, return "Neutral"
  return 'Neutral';
}


function setup() {
  createCanvas(640, 480);
  createVideoCapture();
  createFaceTracker();

  button = createButton('OK'); //create button and button text
  button.style("font-size","12px"); //font size 
  button.position(215,170); //position of button (x, y)
  button.size(60,30); //size of button (width, height)
  button.style("color","black") //text color
  button.style("background-color","#42f57b"); //color of button
  button.mousePressed(sceneChange); //when button is pressed the scene change from 0 -> 1

}

function draw() {
  if (scene == 0){
    background(255);
    push()
    noStroke ();
    fill(170)
    rect(100, 100, 300, 150) 
    stroke(0);
    fill(160);
    rect(100, 100, 300, 30)
    rect(100, 100, 20, 20)
    strokeWeight(2) //linjer  
    line(100, 100, 120, 120) //the cross in escape box
    line(120, 100, 100, 120) //the cross in escape box
    strokeWeight(1) //strokeweight of text
    fill(0) //sort text
    textSize(14) //text størrelse
    text("DO YOU WANT TO ACCEPT COOKIES",125, 120) //text x, y
    pop()
  }

  else if (scene == 1){
  image(videoCapture, 0, 0, 640, 480);
  let positions = faceTracker.getCurrentPosition();
  // Check the availability of face tracking
  if (positions.length > 0) {
    // Get the dominant emotion based on the detected landmarks
    let emotion = getEmotion(positions);

    // Display the detected emotion
    fill(255);
    textSize(20);
    textAlign(CENTER, TOP);
    text('Emotion: ' + emotion, width / 2, 10);
  } else {
  // No face detected
  fill(255);
  textSize(20);
  textAlign(CENTER, TOP);
  text('No face detected', width / 2, 10);
}
}
}

function sceneChange(){ 
  scene = 1;
  button.hide();
}