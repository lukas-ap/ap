# MiniX4 - Data gathering
![Eksempel på miniX4](https://gitlab.com/lukas-ap/ap/-/raw/main/MiniX4/moodz.mp4)

[RunMe](https://lukas-ap.gitlab.io/ap/MiniX4)

[ReadMe](https://gitlab.com/lukas-ap/ap/-/blob/main/MiniX4/sketch.js?ref_type=heads)

### **What have i produced?**
In this MiniX, I have created a simple facial mood recoqnition software, that through the CLMtracker, uses points on your face, to try an calculate your current mood.

Trying to create a working facial mood recoqnition this low fidelity turned out to be more problematic than first expected. The points on the face, is easily affected by the light, so the conditions need to be close to perfect to get the intended result, another problem turned out to bethe importance of the x- and y-values of the points on your face, forcing the person to be centered in the frame, without tilting your head for this to work.

However all in all, under the right circumstances i have created a piece of software that is able to recognize if a person is 'happy' (smiling), neutral, or 'sad' (frowning). This code can definetly be improved in the future, but has been very interesting to work on.

### **My code**
For my code i started by setting up a video capture and face tracker, and displaying it on a 640x480 canvas. The canvas starts with a 'pop-up' displaying 'Do you want to accept cookies?', with a button 'ok' that, if pressed, changes the scene from 0, to 1, which opens your camera.

To check if the person is 'happy', the code looks at my happyLandmarks, which are the points 44, 50 and 57 on the facetracker and checks if the landmark 44, y value and the landmark 50 coordinate y value is lower than the 58 y-value, and will then display 'Happy'

To check if the person is frowning ('sad') the code checks if the point 44 is lower than 57 and then displaying 'Sad'

If none of the conditions are met, the code returns 'Neutral'

![Eksempel på reference](https://gitlab.com/lukas-ap/ap/-/raw/main/MiniX4/reference.png)

### **Reflections**
Creating this facial recognition system has been both a challenging and captivating journey. Working within the constraints of my coding abilities and the limitations of CLMTrackers, which only relies on facial points for mood recognition has made this process tidious and time-consuming to get the best results possible. 

But the fact that that I've managed to develop a functional program capable of recoqnizing if a person is smiling, or frowning, in only a week can be very scary, when you think about how much of our data we willingly accept to give out to different companies, without realizing how much they can do with it. 


___


### **References**
https://www.auduno.com/clmtrackr/docs/reference.html

https://p5js.org/reference/